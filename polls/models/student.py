from . import Base
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Table,
)
from sqlalchemy.orm import relationship

STUDENTS_EXAMS = Table(
    'students_exams', Base.metadata,
    Column('student_id', Integer, ForeignKey('students.id')),
    Column('exam_id', Integer, ForeignKey('exams.id'))
)


class Student(Base):
    __tablename__ = 'students'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(1024))
    exams = relationship(
        'Exam',
        secondary=STUDENTS_EXAMS,
        back_populates='attendees'
    )

    def __json__(self,request):
        return {'id': self.id, 'name': self.name}