from . import Base

from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
)


class Question(Base):
    __tablename__ = "questions"
    id = Column(Integer, primary_key=True)
    phrase = Column(String(1024))
    exam_id = Column(Integer, ForeignKey("exams.id"))

    def __init__(self, ph=None):
        self.phrase = ph

    def __json__(self):
        return {self.id, self.phrase}


class Option(Base):
    __tablename__ = "options"
    id = Column('id',Integer, primary_key=True)
    option = Column('option',String(1024))
    question_id = Column(Integer, ForeignKey("questions.id"))
    def __init__(self,option=None,id=None,question_id=None):
        self.option = option
        self.id = id
        self.question_id = question_id
    def __json__(self):
        return {self.id, self.option, self.question_id}
