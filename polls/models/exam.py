from . import Base
from sqlalchemy import (
    Column,
    Integer,
    String,
)
from sqlalchemy.orm import relationship
from polls.models.student import STUDENTS_EXAMS


class Exam(Base):
    __tablename__ = "exams"
    id = Column(Integer, primary_key=True)
    name = Column(String(1024))
    questions = relationship("Question")
    attendees = relationship(
        'Student',
        secondary=STUDENTS_EXAMS,
        back_populates='exams'
    )

    def __json__(self, request):
        return {'id': self.id, 'name': self.name}
