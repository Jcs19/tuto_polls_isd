"""Main entry point
"""
from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include("cornice")
    config.scan("polls.views")
    config.include("polls.models")
    return config.make_wsgi_app()