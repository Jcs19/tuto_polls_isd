import pyramid
from cornice.resource import resource, view
from polls.models.exam import Exam


@resource(collection_path="/exams", path="/exam/{id}")
class ExamRessource:
    def __init__(self, request):
        self.request = request

    @view(renderer='json')
    def collection_get(self):
        """
        Method to get all exams
        :return: exams collection
        """
        return Exam.query().all()

    @view(renderer='json')
    def get(self):
        """
        method to get One exam
        :return: an exam
        """
        try:
            id_ = int(self.request.matchdict['id'])
        except ValueError:
            raise pyramid.httpexceptions.HTTPNotAcceptable('Id not an integer')
        return Exam.get_by_id(id_)

    @view(renderer="json", accept="text/json")
    def collection_post(self):
        """
        Method for adding an Exam
        """
        exam = Exam()
        exam.name = self.request.POST['name']
        exam.add()
        return True
